import com.xiaoyu.code.AdminDataSource;
import com.xiaoyu.code.CommonJdbcExector;
import com.xiaoyu.code.IDynamicDataSourceDao;
import org.apache.ibatis.jdbc.SQL;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author muxiaoyu
 * @create 2017-03-01 16:24
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/spring/applicationContext*.xml")
public class TestDataSource {
    @Autowired
    private IDynamicDataSourceDao dynamicDataSourceDao;

    public static void main(String[] args) {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:/spring/applicationContext*.xml");
        IDynamicDataSourceDao dynamicDataSourceDao = (IDynamicDataSourceDao) applicationContext.getBean("com.xiaoyu.code.IDynamicDataSourceDao");
        List<AdminDataSource> adminDataSources = dynamicDataSourceDao.select(new AdminDataSource());
        System.out.println(adminDataSources);
    }

    @Test
    public void test01() throws Exception {
        AdminDataSource adminDataSource = new AdminDataSource();
        adminDataSource.setName("dsmysql");
        SQL sql = new SQL().SELECT("*").FROM("user");
        System.out.println(sql.toString());
        AdminDataSource select = dynamicDataSourceDao.selectOne(adminDataSource);
        System.out.println(select);
        CommonJdbcExector commonJdbcExector = new CommonJdbcExector();
       // List<User> users = commonJdbcExector.select(select, sql.toString());
        System.out.println("users");
    }
}
