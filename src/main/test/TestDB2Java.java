import com.xiaoyu.code.AdminDataSource;
import com.xiaoyu.code.CommonJdbcExector;
import com.xiaoyu.code.DB2Entity.GenEntityMysql;
import com.xiaoyu.code.IDynamicDataSourceDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.lang.reflect.Field;

/**
 * ${DESCRIPTION}
 *
 * @author muxiaoyu
 * @create 2017-03-07 10:25
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/applicationContext.xml")
public class TestDB2Java {
    @Autowired
    private IDynamicDataSourceDao dynamicDataSourceDao;

    @Test
    public void test01() {
        AdminDataSource adminDataSource = new AdminDataSource();
        adminDataSource.setName("dssqlserver");
        AdminDataSource dataSource = dynamicDataSourceDao.selectOne(adminDataSource);
        GenEntityMysql genEntityMysql = new GenEntityMysql();
        genEntityMysql.GenerateEntityMysql(dataSource);
    }

    @Test
    public void test02() {
        CommonJdbcExector commonJdbcExector = new CommonJdbcExector();
        AdminDataSource adminDataSource = new AdminDataSource();
        adminDataSource.setName("dsmysql");
        AdminDataSource dataSource = dynamicDataSourceDao.selectOne(adminDataSource);
        try {
            Object o = commonJdbcExector.selectOne(dataSource, "select * from user");
            System.out.println(o);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
