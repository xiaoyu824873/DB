package com.xiaoyu.code;

import com.github.abel533.mapper.Mapper;

/**
 * ${DESCRIPTION}
 *
 * @author muxiaoyu
 * @create 2017-03-01 16:54
 **/
public interface IDynamicDataSourceDao extends Mapper<AdminDataSource>{

}
