package com.xiaoyu.code;

import javax.persistence.Table;

/**
 * ${DESCRIPTION}
 *
 * @author muxiaoyu
 * @create 2017-03-01 16:49
 **/
@Table(name = "t_dataSource")
public class AdminDataSource {
    private String driverClass;
    private String url;
    private String user;
    private String password;
    private Integer id;
    private String name;

    public String getDriverClass() {
        return driverClass;
    }

    public void setDriverClass(String driverClass) {
        this.driverClass = driverClass;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "AdminDataSource{" +
                "driverClass='" + driverClass + '\'' +
                ", url='" + url + '\'' +
                ", user='" + user + '\'' +
                ", password='" + password + '\'' +
                ", id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
