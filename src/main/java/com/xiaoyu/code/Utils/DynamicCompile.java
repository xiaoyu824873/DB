package com.xiaoyu.code.Utils;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.FileFilter;
import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author muxiaoyu
 * @create 2017-03-07 10:48
 **/
public class DynamicCompile {

    public void compile(String path) throws Exception {
        //动态编译
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

        int flag = compiler.run(null, null, null, path);
        System.out.println(flag == 0 ? "编译成功" : "编译失败");
        //获取.class的全路径
        String clazzpath = path.substring(0, path.length() - 4) + "class";
        int split = clazzpath.lastIndexOf("/");
        //获取.class名称
        String clazzname = clazzpath.substring(split, clazzpath.length());
        File file = new File("E:\\development\\code\\svn\\db\\target\\classes\\com\\xiaoyu\\entity" + clazzname);
        file.createNewFile();
        FileUtil.copy(clazzpath, file.getPath());
    }

}
