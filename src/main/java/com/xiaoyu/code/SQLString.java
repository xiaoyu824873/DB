package com.xiaoyu.code;

import org.apache.ibatis.jdbc.SQL;

/**
 * ${DESCRIPTION}
 *
 * @author muxiaoyu
 * @create 2017-03-02 16:19
 **/
public class SQLString {
    public String deleteUserSql(Integer id){
        return new SQL().DELETE_FROM("t_student").WHERE("id=#{id}").toString();
    }
    public String deletePersonSql() {
        return new SQL() {{
            DELETE_FROM("PERSON");
            WHERE("ID = 666");
        }}.toString();
    }
    public static void main(String[] args) {
        System.out.println(new SQLString().insertPersonSql());
    }

    public String insertPersonSql() {
        String sql = new SQL()
                .INSERT_INTO("PERSON")
                .VALUES("ID, FIRST_NAME", "#{id}, #{firstName}")
                .VALUES("LAST_NAME", new String("'xiaoyu'"))
                .toString();
        return sql;
    }
}
