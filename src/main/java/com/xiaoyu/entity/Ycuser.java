package com.xiaoyu.entity;

/**
 * ycuser 实体类
 * Wed Mar 08 13:07:44 CST 2017 xy1122
 */


public class Ycuser {
    private int id;
    private String loginid;
    private int logintype;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public String getLoginid() {
        return loginid;
    }

    public void setLogintype(int logintype) {
        this.logintype = logintype;
    }

    public int getLogintype() {
        return logintype;
    }
}

